//
//  Proposal.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/1/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import Foundation

// MARK: - Proposal Model

struct Proposal {
    
    // MARK: - Declarations
    
    var title: String?
    var proponent: String?
    
    var strengthIndex: Int
    var weaknessIndex: Int
    var opportunityIndex: Int
    var threatIndex: Int
    
    // MARK: Initializers
    
    init() {
        self.strengthIndex = 0
        self.weaknessIndex = 0
        self.opportunityIndex = 0
        self.threatIndex = 0
    }
    
    // MARK: - Functions

    // Returns the index of the specified attribute.
    func index(of attribute: Attribute) -> Int {
        switch attribute {
        case .strength:
            return strengthIndex
        case .weakness:
            return weaknessIndex
        case .opportunity:
            return opportunityIndex
        case .threat:
            return threatIndex
        }
    }
    
    // Returns the choices for the specified attribute.
    func choices(for attribute: Attribute) -> [String]? {
        guard let choices = choices[attribute]
            else {
                print("\nFailed to get choices for specified attribute.\n")
                return nil
        }
        
        return choices.map { $0.text }
    }
    
    // Returns the text specified attribute's selected choice.
    func choice(of attribute: Attribute) -> String? {
        if let choices = choices[attribute] {
            let i = index(of: attribute)
            return choices[i].text
        } else {
            print("\nFailed to get choice. Choices for specified attribute is nil.\n")
            return nil
        }
    }
    
    // Returns the code of the specified attribute's selected choice.
    func code(of attribute: Attribute) -> Code? {
        if let choices = choices[attribute] {
            let i = index(of: attribute)
            return choices[i].code
        } else {
            print("\nFailed to get code. Choices for specified attribute is nil.\n")
            return nil
        }
    }
    
    // Returns the equivalent factor for the specified attribute.
    // It gets the attribute's code. 
    // Then, it uses the code as a key to get the corresponding factor in the dictionary 'factors' (See 'factors' under constants below.)
    func factor(for attribute: Attribute) -> String? {
        guard let code = code(of: attribute),
            let factor = factors[code]
            else {
                print("\nFailed to get factor for attribute. Code for attribute is nil.\n")
                return nil
        }
        return factor
    }
    
    // Returns the equivalent ranks for the specified attribute.
    // It gets the attribute's code.
    // Then, it uses the code as a key to get the corresponding dictionary of [organizations and ranks] in 'ranks' (See 'ranks' under constants below.)
    func ranks(for attribute: Attribute) -> [Organization: Int]? {
        guard let code = code(of: attribute),
            let ranks = ranks[code]
            else {
                print("\nFailed to get ranks for attribute. Attribute's code or rank for code is nil.\n")
                return nil
        }
        return ranks
    }
    
    // Returns the equivalent overall for the specified attribute.
    // It gets the attribute's code.
    // Then, it uses the code as a key to get the corresponding overall in the dictionary 'overalls' (See 'overalls' under constants below.)
    func overall(for attribute: Attribute) -> Double? {
        if let code = code(of: attribute),
        let overall = overalls[code] {
            return overall
        } else {
            print("\nNo value in 'overalls' for specified attribute.\n")
            return nil
        }
    }
    
    // Returns the sum of the overall for the 2 specified attributes.
    func relationship(between attribute1: Attribute, and attribute2: Attribute) -> Double? {
        guard let overall1 = overall(for: attribute1),
            let overall2 = overall(for: attribute2) else {
                print("One or both of the overalls for the attributes returned nil.")
                return nil
        }
        return overall1 + overall2
    }

    // MARK: - Constants

    // Contains the choices and their corresponding codes for each attribute.
    // Example: choices[Attribute.strength] or choices[.strength] for short, returns the array of choices and code for strength.
    // choices[.strength][0] will return the first element: ("Ensures stable supply of food, ... the local community", Code.S1)
    // Each element in the array is a tuple: (text: String, code: Code)
    // The first element in the tuple is a String named 'text', the second is the Code named 'code'.
    // The elements in the tuple are named to ease access and readability.
    // Example, to get the code of the 6th choice in weakness:
    //      weaknessChoices = choices[.weakness] ----> returns the texts and codes for Attribute.weakness
    //      sixthWeaknessChoice = weaknessChoices[5] ----> returns the 6th element in the weakness choices: ("Low ... members", Code.W2)
    //      sixthWeaknessCode = sixthWeaknessChoice.code ----> returns Code.W2
    let choices: [Attribute: [(text: String, code: Code)]] = [
        Attribute.strength: [
            ("Ensures stable supply of food, medicine, building material for the local community", Code.S1),
            ("Offers socio-economic incentives for the local community", Code.S1),
            ("Provides employment opportunity for the local community", Code.S1),
            ("Adopts appropriate technology for long term PL conservation", Code.S2),
            ("Facilitates environmental education/information on the PL", Code.S2),
            ("Reduces cost in PL conservation management", Code.S2),
            ("Empowers local community to undertake conservation effort in the PL", Code.S3),
            ("Encourages high level of biodiversity conservation attitude among the local community", Code.S3),
            ("Integrates capacity-building and local community’s co-ownership mindset", Code.S3),
            ("Provides livelihood improvement and income generation for the local community", Code.S3)
        ],
        Attribute.weakness: [
            ("Insufficient income generation of the local community from forest products", Code.W1),
            ("Limited employment opportunity for the local community", Code.W1),
            ("Restriction to efficient market access thereby reducing income from forest products", Code.W1),
            ("Lack of commitment among local community members", Code.W2),
            ("Limited conservation awareness and skills of the local community", Code.W2),
            ("Low conservation value among local community members", Code.W2),
            ("Absence of monitoring and quick response to rising/reported problems/issues", Code.W3),
            ("Incomplete legal recognition of the PL (No RA, PP only)", Code.W3),
            ("Lack of sincere cooperation by some stakeholders", Code.W3),
            ("Unclear implementation of guiding policies as perceived by the local community", Code.W3),
            ("Insufficient support activities to the local community (trainings, farm technology, etc.)", Code.W4),
            ("Mismatched management activities/programs and available resources/funds", Code.W4),
            ("Deficient financial/human resources to support implementation of the conservation program", Code.W4)
        ],
        Attribute.opportunity: [
            ("Improves functional and open dealings between local community and management", Code.O1),
            ("Offers greater socio-economic stability for the local community", Code.O1),
            ("Restores mutual appreciation and recognition of the stakeholders’ importance in conservation", Code.O1),
            ("Facilitates food security and income for the local community", Code.O2),
            ("Integrates local community’s interest for alternative and/or more sustainable PL livelihood", Code.O2),
            ("Offers employment prospect for the local community", Code.O2),
            ("Improved policies appropriate to current realities and future opportunities", Code.O3),
            ("Strengthened conservation partnership with different stakeholders", Code.O3),
            ("Supplemented sustainable use of forest resources by the local community", Code.O3),
            ("Facilitated development of local community’s own conservation strategy", Code.O3)
        ],
        Attribute.threat: [
            ("Local community’s unsustainable forest resources utilization", Code.T1),
            ("Rampant illegal activities in the PL", Code.T1),
            ("Demotivated and passive stakeholders", Code.T2),
            ("Interest collision among local community members", Code.T2),
            ("Possible conflict between/within local communities and other stakeholders", Code.T2),
            ("Restricted participation of the local community to decision-building", Code.T2),
            ("Changing political scenarios", Code.T3),
            ("Institutional corruption", Code.T3),
            ("PAMB’s loss of political strength to win out over other’s interest (done deals and faits accompli)", Code.T3),
            ("Unstable government regulations and policies", Code.T3)
        ]
    ]
    
    // Contains the corresponding factors for each code.
    // This is a dictionary of type [Code: String]. Meaning, if you supply the Code as a key, it will return its corresponding String value.
    // Example, factors[Code.O3] of factors[.O3] will return "Enhanced PL management"
    let factors: [Code: String] = [
        Code.S1: "Needs fulfillment of the local community",
        Code.S2: "PL management benefit",
        Code.S3: "Ideal institutional arrangement",
        Code.W1: "Inadequate benefit for the local community",
        Code.W2: "Low level conservation perception",
        Code.W3: "Weak management control in regulation enforcement",
        Code.W4: "Management limitations",
        Code.O1: "Improved stakeholders’ relations",
        Code.O2: "Reduced poverty in the local community",
        Code.O3: "Enhanced PL management",
        Code.T1: "Continuing biodiversity loss",
        Code.T2: "Potential failure of the conservation effort",
        Code.T3: "Uncertainty associated with decision-making process"
    ]

    // Contains the ranks given by the organizations identified by each code.
    // Example, to get NGO rank for Code.W1: ranks[.W1][.NGO]
    let ranks: [Code: [Organization: Int]] = [
        Code.S1: [
            Organization.LG: 1,
            Organization.NGO: 3,
            Organization.LGU: 2,
            Organization.PAMB: 3,
            Organization.Group: 2
        ],
        Code.S2: [
            Organization.LG: 3,
            Organization.NGO: 2,
            Organization.LGU: 3,
            Organization.PAMB: 2,
            Organization.Group: 3
        ],
        Code.S3: [
            Organization.LG: 2,
            Organization.NGO: 1,
            Organization.LGU: 1,
            Organization.PAMB: 1,
            Organization.Group: 1
        ],
        Code.W1: [
            Organization.LG: 2,
            Organization.NGO: 4,
            Organization.LGU: 4,
            Organization.PAMB: 4,
            Organization.Group: 4
        ],
        Code.W2: [
            Organization.LG: 4,
            Organization.NGO: 3,
            Organization.LGU: 3,
            Organization.PAMB: 3,
            Organization.Group: 3
        ],
        Code.W3: [
            Organization.LG: 3,
            Organization.NGO: 2,
            Organization.LGU: 1,
            Organization.PAMB: 2,
            Organization.Group: 2
        ],
        Code.W4: [
            Organization.LG: 1,
            Organization.NGO: 1,
            Organization.LGU: 2,
            Organization.PAMB: 1,
            Organization.Group: 1
        ],
        Code.O1: [
            Organization.LG: 2,
            Organization.NGO: 1,
            Organization.LGU: 1,
            Organization.PAMB: 1,
            Organization.Group: 1
        ],
        Code.O2: [
            Organization.LG: 1,
            Organization.NGO: 3,
            Organization.LGU: 3,
            Organization.PAMB: 3,
            Organization.Group: 3
        ],
        Code.O3: [
            Organization.LG: 3,
            Organization.NGO: 2,
            Organization.LGU: 2,
            Organization.PAMB: 2,
            Organization.Group: 2
        ],
        Code.T1: [
            Organization.LG: 3,
            Organization.NGO: 3,
            Organization.LGU: 3,
            Organization.PAMB: 2,
            Organization.Group: 3
        ],
        Code.T2: [
            Organization.LG: 2,
            Organization.NGO: 2,
            Organization.LGU: 2,
            Organization.PAMB: 3,
            Organization.Group: 2
        ],
        Code.T3: [
            Organization.LG: 1,
            Organization.NGO: 1,
            Organization.LGU: 1,
            Organization.PAMB: 1,
            Organization.Group: 1
        ]
    ]

    
    // Contains the overalls for each code.
    let overalls: [Code: Double] = [
        Code.S1: 0.0687,
        Code.S2: 0.0564,
        Code.S3: 0.1752,
        Code.W1: 0.018,
        Code.W2: 0.0184,
        Code.W3: 0.055,
        Code.W4: 0.1088,
        Code.O1: 0.1623,
        Code.O2: 0.0651,
        Code.O3: 0.0729,
        Code.T1: 0.0284,
        Code.T2: 0.0432,
        Code.T3: 0.1288
    ]
}
