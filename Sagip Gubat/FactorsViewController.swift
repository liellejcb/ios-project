//
//  FactorsViewController.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/10/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import UIKit

class FactorsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var proponentLabel: UILabel!
    @IBOutlet weak var strengthFactorLabel: UILabel!
    @IBOutlet weak var weaknessFactorLabel: UILabel!
    @IBOutlet weak var opportunityFactorLabel: UILabel!
    @IBOutlet weak var threatFactorLabel: UILabel!
    
    var proposal: Proposal!
    var exportButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.title = "Factors"
        titleLabel.text = "\"\(proposal.title! )\""
        proponentLabel.text = "by \(proposal.proponent! )"
        
        exportButton = UIBarButtonItem(title: "Export", style: .plain, target: self, action: #selector(export))
        self.tabBarController?.navigationItem.rightBarButtonItem = exportButton
        
        strengthFactorLabel.text = proposal.factor(for: .strength)
        weaknessFactorLabel.text = proposal.factor(for: .weakness)
        opportunityFactorLabel.text = proposal.factor(for: .opportunity)
        threatFactorLabel.text = proposal.factor(for: .threat)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func swiped(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController?.selectedIndex = 1
            self.tabBarController?.title = "Ranks"
        }
    }
    
    func export() {
        let strengthRank = proposal.ranks(for: .strength)!
        let weaknessRank = proposal.ranks(for: .weakness)!
        let opportunityRank = proposal.ranks(for: .opportunity)!
        let threatRank = proposal.ranks(for: .threat)!
        
        let data =
            "Proposed strategy: \(proposal.title!)"
            + "\nProponent: \(proposal.proponent!)"
            
            + "\n\nA. Factors"
            + "\nThe proposed strategy can build upon on: \(proposal.factor(for: .strength)!)"
            + "\nThe proposed strategy needs to address: \(proposal.factor(for: .weakness)!)"
            + "\nThe proposed strategy can potentially provide: \(proposal.factor(for: .opportunity)!)"
            + "\nThe proposed strategy needs to be aware of: \(proposal.factor(for: .threat)!)"
                
            + "\n\nB. Stakeholders' prioritized scores (Rank)"
            + "\nSWOT \t\t\tLG \t\tNGO \tLGU \t\tPAMB \tGroup"
            + "\nStrength \t\t\t\(strengthRank[.LG]!) \t\t\(strengthRank[.NGO]!) \t\t\(strengthRank[.LGU]!) \t\t\(strengthRank[.PAMB]!) \t\t\(strengthRank[.Group]!)"
            + "\nWeakness \t\t\(weaknessRank[.LG]!) \t\t\(weaknessRank[.NGO]!) \t\t\(weaknessRank[.LGU]!) \t\t\(weaknessRank[.PAMB]!) \t\t\(weaknessRank[.Group]!)"
            + "\nOpportunity \t\t\(opportunityRank[.LG]!) \t\t\(opportunityRank[.NGO]!) \t\t\(opportunityRank[.LGU]!) \t\t\(opportunityRank[.PAMB]!) \t\t\(opportunityRank[.Group]!)"
            + "\nThreat \t\t\t\(threatRank[.LG]!) \t\t\(threatRank[.NGO]!) \t\t\(threatRank[.LGU]!) \t\t\(threatRank[.PAMB]!) \t\t\(threatRank[.Group]!)"
            
            + "\n\nC. Relationship between any two SWOT factors"
            + "\nStrength & Opporunity (SO): \t\t\(proposal.relationship(between: .strength, and: .opportunity)!)"
            + "\nWeakness & Opporunity (WO): \t\(proposal.relationship(between: .weakness, and: .opportunity)!)"
            + "\nStrength & Threat (ST): \t\t\t\(proposal.relationship(between: .strength, and: .threat)!)"
            + "\nWeakness & Threat (WT): \t\t\(proposal.relationship(between: .weakness, and: .threat)!)"
            
            + "\n\n* Strength: \t\t\(proposal.overall(for: .strength)!)"
            + "\n* Weakness: \t\t\(proposal.overall(for: .weakness)!)"
            + "\n* Opportunity: \t\(proposal.overall(for: .opportunity)!)"
            + "\n* Threat: \t\t\t\(proposal.overall(for: .threat)!)"
        
        let attrs = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor.black]
        let str = NSAttributedString(string: data, attributes: attrs)
        let print = UISimpleTextPrintFormatter(attributedText: str)
        let vc = UIActivityViewController(activityItems: [print], applicationActivities: nil)
        vc.popoverPresentationController?.barButtonItem = exportButton
        present(vc, animated: false, completion: nil)
    }
}
