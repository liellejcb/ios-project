//
//  MainViewController.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 8/31/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var pickerShownConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerHiddenConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var proponentText: UITextField!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var weaknessLabel: UILabel!
    @IBOutlet weak var opportunityLabel: UILabel!
    @IBOutlet weak var threatLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    // MARK: - Properties
    
    let lowPriority: Float = 250
    let highPriority: Float = 750
    
    var selectedAttribute: Attribute!
    var proposal: Proposal!
    var choices: [String]!
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        picker.dataSource = self
        
        for label in [strengthLabel, weaknessLabel, opportunityLabel, threatLabel] {
            let tap = UITapGestureRecognizer(target: self, action: #selector(showPickerViaLabel))
            label?.addGestureRecognizer(tap)
        }

        resetUI()
        choices = []
    }
    
    func showPickerViaLabel(_ sender: UITapGestureRecognizer) {
        let tappedLabel = sender.view as! UILabel
        showPicker(tappedLabel)
    }
    
    // MARK: - Reset
    
    @IBAction func resetUI() {
        proposal = Proposal()
        updateChoiceLabels()
        titleText.text = ""
        titleText.becomeFirstResponder()
        proponentText.text = ""
        hidePicker()
    }
    
    // MARK: - Updates
    
    func updateChoiceLabels() {
        strengthLabel.text = proposal.choice(of: .strength)
        weaknessLabel.text = proposal.choice(of: .weakness)
        opportunityLabel.text = proposal.choice(of: .opportunity)
        threatLabel.text = proposal.choice(of: .threat)
    }
    
    // Sets proposal's index for selected attribute into the selected index in picker.
    func updateProposalAttribute() {
        let selectedChoiceIndex = picker.selectedRow(inComponent: 0)
        switch selectedAttribute! {
        case .strength:
            proposal.strengthIndex = selectedChoiceIndex
        case .weakness:
            proposal.weaknessIndex = selectedChoiceIndex
        case .opportunity:
            proposal.opportunityIndex = selectedChoiceIndex
        case .threat:
            proposal.threatIndex = selectedChoiceIndex
        }
    }
    
    // MARK: - Show/Hide Picker
    
    // Called by the pencil icons to show picker. Each icon has different tags assigned in storyboard.
    // 1st button: 0, 2nd button: 1, 3rd = 2, 4th = 3
    @IBAction func showPicker(_ sender: AnyObject) {
        
        // Check if button's tag is within 0-3. If 0, attribute is strength, 1 = weakness, 2 = opportunity, 3 = threat.
        guard let attribute = Attribute(rawValue: sender.tag)
            else {
                print("\nError with initializing an attribute with the button's tag. "
                    + "Check if button's tag is within 0-3.\n")
                return
        }

        // Change data in picker to show choices for selected question.
        choices = proposal.choices(for: attribute)
        picker.reloadAllComponents()
        
        // Set selected choice in picker to choice previously selected.
        picker.selectRow(proposal.index(of: attribute), inComponent: 0, animated: true)
        
        // Store selected attribute to use when updating proposal's attribute.
        selectedAttribute = attribute
        
        // Set higher priority to constraint showing the picker to make it appear.
        pickerShownConstraint.priority = highPriority
        pickerHiddenConstraint.priority = lowPriority
        
        // Hide keyboard if shown
        titleText.resignFirstResponder()
        proponentText.resignFirstResponder()
        
        // Show the 'Done' bar button item for dismissing picker.
        doneButton.isEnabled = true
        doneButton.title = "Done"
    }
    
    @IBAction func hidePicker() {
        pickerShownConstraint.priority = lowPriority
        pickerHiddenConstraint.priority = highPriority
        
        doneButton.isEnabled = false
        doneButton.title = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "results" {
            let destination = segue.destination as! UITabBarController
            
            let factorsViewController = destination.viewControllers?[0] as! FactorsViewController
            factorsViewController.proposal = proposal
            
            let ranksViewController = destination.viewControllers?[1] as! RanksViewController
            ranksViewController.proposal = proposal
            
            let relationshipsViewController = destination.viewControllers?[2] as! RelationshipsViewController
            relationshipsViewController.proposal = proposal
        }
    }
    
    // Called when Calculate button is pressed
    // For now, just prints the results in the console.
    @IBAction func showResults(_ sender: UIButton) {
        hidePicker()
        
        guard let title = titleText.text,
            let proponent = proponentText.text,
            title != "",
            proponent != ""
            else {
                let alert = UIAlertController(
                    title: "One or more fields are empty",
                    message: "Please enter the proposed strategy's title and proponent name.",
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                    self.dismiss(animated: true)
                    
                    if self.titleText.text == "" {
                        self.titleText.becomeFirstResponder()
                    } else {
                        self.proponentText.becomeFirstResponder()
                    }
                })
                present(alert, animated: true)
                return
        }
        
        proposal.title = title
        proposal.proponent = proponent
        
        performSegue(withIdentifier: "results", sender: sender)

        let strengthRank = proposal.ranks(for: .strength)!
        let weaknessRank = proposal.ranks(for: .weakness)!
        let opportunityRank = proposal.ranks(for: .opportunity)!
        let threatRank = proposal.ranks(for: .threat)!
        
        print(
              "\n\nProposed strategy: \(title)"
            + "\nProponent: \(proponent)"
            
            + "\n\nA. Factor that the proposed strategy: "
            + "\n- can build upon on: \(proposal.factor(for: .strength)!)"
            + "\n- needs to address: \(proposal.factor(for: .weakness)!)"
            + "\n- can potentially provide: \(proposal.factor(for: .opportunity)!)"
            + "\n- needs to be aware of: \(proposal.factor(for: .threat)!)"
            
            + "\n\nB. Stakeholders' prioritized scores (Rank)"
            + "\nSWOT \t\t\tLG \t\tNGO \tLGU \tPAMB \tGroup"
            + "\nStrength \t\t\(strengthRank[.LG]!) \t\t\(strengthRank[.NGO]!) \t\t\(strengthRank[.LGU]!) \t\t\(strengthRank[.PAMB]!) \t\t\(strengthRank[.Group]!)"
            + "\nWeakness \t\t\(weaknessRank[.LG]!) \t\t\(weaknessRank[.NGO]!) \t\t\(weaknessRank[.LGU]!) \t\t\(weaknessRank[.PAMB]!) \t\t\(weaknessRank[.Group]!)"
            + "\nOpportunity \t\(opportunityRank[.LG]!) \t\t\(opportunityRank[.NGO]!) \t\t\(opportunityRank[.LGU]!) \t\t\(opportunityRank[.PAMB]!) \t\t\(opportunityRank[.Group]!)"
            + "\nThreat \t\t\t\(threatRank[.LG]!) \t\t\(threatRank[.NGO]!) \t\t\(threatRank[.LGU]!) \t\t\(threatRank[.PAMB]!) \t\t\(threatRank[.Group]!)"
        
            + "\n\nC. Relationship between any two SWOT factors"
            + "\nStrength & Opporunity (SO): \t\(proposal.relationship(between: .strength, and: .opportunity)!)"
            + "\nWeakness & Opporunity (WO): \t\(proposal.relationship(between: .weakness, and: .opportunity)!)"
            + "\nStrength & Threat (ST): \t\t\(proposal.relationship(between: .strength, and: .threat)!)"
            + "\nWeakness & Threat (WT): \t\t\(proposal.relationship(between: .weakness, and: .threat)!)"
            
            + "\n\n* Strength: \t\(proposal.overall(for: .strength)!)"
            + "\n* Weakness: \t\(proposal.overall(for: .weakness)!)"
            + "\n* Opportunity: \t\(proposal.overall(for: .opportunity)!)"
            + "\n* Threat: \t\t\(proposal.overall(for: .threat)!)"
        )
    }
}

// MARK: - Choices Picker Delegate
// Delegate for the UIPickerView displaying choices
extension MainViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Number of columns
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Number of rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choices.count
    }
    
    // What will happen if a row is selected
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateProposalAttribute()
        updateChoiceLabels()
    }
    
    // What will each row look like
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.text = choices[row]
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.attributedText = NSAttributedString(string: choices[row], attributes: [NSForegroundColorAttributeName: UIColor.white])
        return label
    }
    
    // Height of each row
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
}

// MARK: - TextField Delegate
// Delegate to dismiss keyboard when Next/Done button is pressed.
extension MainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == titleText {
            proponentText.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

