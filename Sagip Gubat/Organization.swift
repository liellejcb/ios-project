//
//  Organization.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/1/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import Foundation

// Enumeration of organizations
enum Organization {
    case LG
    case NGO
    case LGU
    case PAMB
    case Group
}
