//
//  Code.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/1/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import Foundation

// Enumeration of codes
enum Code {
    case S1, S2, S3
    case W1, W2, W3, W4
    case O1, O2, O3
    case T1, T2, T3
}
