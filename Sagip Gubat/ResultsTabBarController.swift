//
//  ResultsTabBarController.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/10/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import UIKit

class ResultsTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        tabBarController.title = viewController.title
    }
}
