//
//  Attribute.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/1/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import Foundation

// Enumeration of the 4 attributes
enum Attribute: Int {
    case strength = 0
    case weakness = 1
    case opportunity = 2
    case threat = 3
}
