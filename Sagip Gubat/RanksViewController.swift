//
//  RanksViewController.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/10/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import UIKit

class RanksViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var proponentLabel: UILabel!
    
    @IBOutlet var strengthRanks: [UILabel]!
    @IBOutlet var weaknessRanks: [UILabel]!
    @IBOutlet var opportunityRanks: [UILabel]!
    @IBOutlet var threatRanks: [UILabel]!
    
    var proposal: Proposal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "\"\(proposal?.title! ?? "Title")\""
        proponentLabel.text = "by \(proposal?.proponent! ?? "Yer name")"
        
        populateRanks(strengthRanks, attribute: .strength)
        populateRanks(weaknessRanks, attribute: .weakness)
        populateRanks(opportunityRanks, attribute: .opportunity)
        populateRanks(threatRanks, attribute: .threat)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func populateRanks(_ labelArray: [UILabel], attribute: Attribute) {
        guard let ranks = (proposal?.ranks(for: attribute))
            else {
                print("\nUnable to fetch ranks for \(attribute). Ranks for \(attribute) returned nil.\n")
                return
        }
        
        for label in labelArray {
            switch label.tag {
            case 0:
                label.text = "\(ranks[.LG] ?? 0)"
            case 1:
                label.text = "\(ranks[.NGO] ?? 0)"
            case 2:
                label.text = "\(ranks[.LGU] ?? 0)"
            case 3:
                label.text = "\(ranks[.PAMB] ?? 0)"
            case 4:
                label.text = "\(ranks[.Group] ?? 0)"
            default:
                break
            }
        }
    }
    
    func swiped(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.tabBarController?.selectedIndex = 2
            self.tabBarController?.title = "Relationships"
        } else if sender.direction == .right {
            self.tabBarController?.selectedIndex = 0
            self.tabBarController?.title = "Factors"
        }
    }
}
