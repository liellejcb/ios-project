//
//  RelationshipsViewController.swift
//  Sagip Gubat
//
//  Created by Lielle Bawar on 9/10/17.
//  Copyright © 2017 dlsud. All rights reserved.
//

import UIKit

class RelationshipsViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var proponentLabel: UILabel!
    @IBOutlet weak var soLabel: UILabel!
    @IBOutlet weak var woLabel: UILabel!
    @IBOutlet weak var stLabel: UILabel!
    @IBOutlet weak var wtLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var weaknessLabel: UILabel!
    @IBOutlet weak var opportunityLabel: UILabel!
    @IBOutlet weak var threatLabel: UILabel!
    
    var proposal: Proposal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "\"\(proposal?.title! ?? "Title")\""
        proponentLabel.text = "by \(proposal?.proponent! ?? "Yer name")"
        
        soLabel.text = "\(proposal?.relationship(between: .strength, and: .opportunity) ?? 0.0000)"
        woLabel.text = "\(proposal?.relationship(between: .weakness, and: .opportunity) ?? 0.0000)"
        stLabel.text = "\(proposal?.relationship(between: .strength, and: .threat) ?? 0.0000)"
        wtLabel.text = "\(proposal?.relationship(between: .weakness, and: .threat) ?? 0.0000)"
        
        strengthLabel.text = "\(proposal?.overall(for: .strength) ?? 0.0000)"
        weaknessLabel.text = "\(proposal?.overall(for: .weakness) ?? 0.0000)"
        opportunityLabel.text = "\(proposal?.overall(for: .opportunity) ?? 0.0000)"
        threatLabel.text = "\(proposal?.overall(for: .threat) ?? 0.0000)"
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func swiped(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .right {
            self.tabBarController?.selectedIndex = 1
            self.tabBarController?.title = "Ranks"
        }
    }
}
